import express from 'express';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'babel-polyfill';
import cors from 'cors';
import env from '../env';
import user from './lib/routes/user';
// import admin  from './app/routes/admin';
// import trip from './app/routes/trip';
// import bus from './app/routes/bus';
// import booking from './app/routes/booking';

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/api/v1', user);
// app.use("/api/v1", admin);
// app.use("/api/v1", trip);
// app.use("/api/v1", bus);
// app.use("/api/v1", booking);

app.listen(env.port).on('listening', () => {
  console.log(`🚀 are live on ${env.port}`);
});

export default app;
