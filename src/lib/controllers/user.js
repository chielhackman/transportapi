import bcrypt from 'bcryptjs';
import UserService from '../services/user';
import {
  hashPassword,
  comparePassword,
  isValidEmail,
  validatePassword,
  isEmpty,
  generateUserToken,
} from '../helpers/validations';
import {
  errorMessage, successMessage, status,
} from '../helpers/status';

const createUser = async (req, res) => {
  const user = req.body;
  const { plainPassword } = user.password;
  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds);
  user.password = bcrypt.hashSync(plainPassword, salt);
  try {
    const createdUser = await UserService.addUser(user);
    const token = generateUserToken(createdUser.email,
      createdUser.id,
      createdUser.admin,
      createdUser.firstName,
      createdUser.lastName);
    successMessage.data = createdUser;
    successMessage.data.token = token;
  } catch (error) {
    errorMessage.error = 'Failed to create a user.';
    return res.status(status.error).send(errorMessage);
  }
  return res.status(status.created).send(successMessage);
};

export {
  createUser,
};
