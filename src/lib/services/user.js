import bcrypt from 'bcryptjs';

const db = require('../models');

const User = db.user;

class UserService {
  static async addUser(user) {
    // const user = newUser;
    // const { password } = user.password;
    // const saltRounds = 10;
    // const salt = bcrypt.genSaltSync(saltRounds);
    // user.password = bcrypt.hashSync(password, salt);
    // const hashPassword = (password) => bcrypt.hashSync(password, salt);
    // const hashedPassword = hashPassword(password);
    // console.log(hashedPassword);
    // user.password = hashedPassword;
    // console.log(user);
    // if (password.length <= 5) {
    //   return;
    // }

    // eslint-disable-next-line no-useless-catch
    try {
      return await User.create(user);
    } catch (error) {
      throw error;
    }
  }
}

export default UserService;
