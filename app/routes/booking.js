import express from "express";
import { createBooking, getAllBookings, deleteBooking, updateBookingSeat } from "../controllers/booking";
import verifyAuth from "../middlewares/verifyAuth";

const router = express.Router();

router.post("/bookings", verifyAuth, createBooking);
router.get("/bookings", verifyAuth, getAllBookings);
router.delete("/bookings/:booking_id", verifyAuth, deleteBooking);
router.put("/bookings/:booking_id", verifyAuth, updateBookingSeat);

export default router;