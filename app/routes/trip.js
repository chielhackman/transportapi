import express from "express";
import {
  createTrip, getAllTrips, cancelTrip, filterTripByOrigin, filterTripByDestination
} from "../controllers/trip";
import verifyAuth from "../middlewares/verifyAuth";

const router = express.Router();

router.post("/trips", verifyAuth, createTrip);
router.get("/trips", verifyAuth, getAllTrips);
router.patch("/trips/:trip_id", verifyAuth, cancelTrip);
router.get("/trips/origin", verifyAuth, filterTripByOrigin);
router.get("/trips/destination", verifyAuth, filterTripByDestination);

export default router;