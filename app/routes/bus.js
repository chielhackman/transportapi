import express from "express";
import { addBusDetails, getAllBuses } from "../controllers/bus";
import verifyAuth from "../middlewares/verifyAuth";

const router = express.Router();

router.post("/buses", verifyAuth, addBusDetails);
router.get("/buses", verifyAuth, getAllBuses);

export default router;