import pool from "./pool";

pool.on("connect", () => {
  console.log("connected to db");
});

// Create User table
const createUserTable = () => {
  const userCreateQuery = `CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(100) UNIQUE NOT NULL,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    password VARCHAR(100) NOT NULL,
    is_admin BOOLEAN,
    created_at DATE NOT NULL
  )`;

  pool.query(userCreateQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Create Bus table
const createBusTable = () => {
  const busCreateQuery = `CREATE TABLE IF NOT EXISTS bus (
    id SERIAL PRIMARY KEY,
    number_plate VARCHAR(100) NOT NULL,
    manufacturer VARCHAR(100) NOT NULL,
    model VARCHAR(100) NOT NULL,
    year VARCHAR(10) NOT NULL,
    capacity INTEGER NOT NULL,
    created_at DATE NOT NULL
  )`;
  
  pool.query(busCreateQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Create Trip table
const createTripTable = () => {
  const tripCreateQuery = `CREATE TABLE IF NOT EXISTS trip (
    id SERIAL PRIMARY KEY,
    bus_id int4 REFERENCES bus(id) ON DELETE CASCADE,
    origin VARCHAR(300) NOT NULL,
    destination VARCHAR(300) NOT NULL,
    trip_date DATE NOT NULL,
    fare float NOT NULL,
    status float DEFAULT(1.00),
    created_at DATE NOT NULL
  )`;
  
  pool.query(tripCreateQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Create Booking table
const createBookingTable = () => {
  const bookingCreateQuery = `CREATE TABLE IF NOT EXISTS booking (
    id SERIAL,
    trip_id int4 REFERENCES trip(id) ON DELETE CASCADE,
    user_id int4 REFERENCES users(id) ON DELETE CASCADE,
    bus_id int4 REFERENCES bus(id) ON DELETE CASCADE,
    trip_date DATE,
    seat_number INTEGER UNIQUE,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    created_at DATE NOT NULL,
    PRIMARY KEY (id, trip_id, user_id)
  )`;

  pool.query(bookingCreateQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Drop User table
const dropUserTable = () => {
  const usersDropQuery = `DROP TABLE IF EXISTS users`;
  pool.query(usersDropQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Drop Bus table
const dropBusTable = () => {
  const busDropQuery = `DROP TABLE IF EXISTS bus`;
  pool.query(busDropQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Drop Trip table
const dropTripTable = () => {
  const tripDropQuery = `DROP TABLE IF EXISTS trip`;
  pool.query(tripDropQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Drop Booking table
const dropBookingTable = () => {
  const bookingDropQuery = `DROP TABLE IF EXISTS booking`;
  pool.query(bookingDropQuery)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

// Create all tables
const createAllTables = () => {
  createUserTable();
  createBusTable();
  createTripTable();
  createBookingTable();
};

// Drop all tables
const dropAllTables = () => {
  dropUserTable();
  dropBusTable();
  dropTripTable();
  dropBookingTable();
};

pool.on("remove", () => {
  console.log("client removed");
  process.exit(0);
});

export {
  createAllTables,
  dropAllTables,
};

require("make-runnable");