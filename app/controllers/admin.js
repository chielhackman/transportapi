import moment from "moment";
import dbQuery from "../db/dev/dbQuery";
import {
  isValidEmail,
  validatePassword,
  isEmpty,
  hashPassword,
  generateUserToken
} from "../helpers/validations";
import {
  errorMessage, successMessage, status
} from "../helpers/status";

const createAdmin = async (req, res) => {
  const {
    email, first_name, last_name, password
  } = req.body;

  const { is_admin } = req.user;
  const isAdmin = true;
  const created_at = moment(new Date());

  if (!is_admin === false) {
    errorMessage.error = "Unauthorized";
    return res.status(status.bad).send(errorMessage);
  }

  // This isn't a good approach, if there are added more attributes to the admin the list will grow
  // Think about passing an array to isEmpty method and loop through it and give back the empty
  // attribute so we can display a proper error message
  if (isEmpty(email) || isEmpty(password) || isEmpty(first_name) || isEmpty(last_name)) {
    errorMessage.error = "Email, password, first name and last name field can not be empty.";
    return res.status(status.bad).send(errorMessage);
  }

  if (!isValidEmail(email)) {
    errorMessage.error = "Provided email is not valid.";
    return res.status(status.bad).send(errorMessage);
  }

  if (!validatePassword(password)) {
    errorMessage.error = "Password must be more than 5 characters.";
    return res.status(status.bad).send(errorMessage);
  }

  const hashedPassword = hashPassword(password);
  const createUserQuery = `INSERT INTO
    users(email, password, first_name, last_name, is_admin, created_at)
    VALUES($1, $2, $3, $4, $5, $6)
    returning *`;
  const values = [
    email,
    hashedPassword,
    first_name,
    last_name,
    is_admin,
    created_at
  ];

  try {
    const { rows } = await dbQuery.query(createUserQuery, values);
    const dbResponse = rows[0];
    delete dbResponse.password;
    const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
    successMessage.data = dbResponse;
    successMessage.data.token = token;
    return res.status(status.created).send(successMessage);
  } catch (error) {
    if (error.routine === "_bt_check_unique") {
      errorMessage.error = "There already is an admin account with the provided email.";
      return res.status(status.conflict).send(errorMessage);
    }
  }
};

const updateUserToAdmin = async (req, res) => {
  const { id } = req.params;
  const { isAdmin } = req.body;

  const { is_admin } = req.user;
  if (!is_admin === true) {
    errorMessage.error = "Unauthorized";
    return res.status(status.bad).send(errorMessage);
  }
  if (isAdmin === "") {
    errorMessage.error = "No admin status provided";
    return res.status(status.bad).send(errorMessage);
  }
  const findUserQuery = `SELECT * FROM users WHERE id=$1`;
  const updateUser = `UPDATE users SET is_admin=$1 WHERE id=$2 returning *`;
  try {
    const { rows } = await dbQuery.query(findUserQuery, [id]);
    const dbResponse = rows[0];
    if (!dbResponse) {
      errorMessage.error = "Can not find provided user.";
      return res.status(status.notfound).send(errorMessage);
    }
    const values = [
      isAdmin,
      id
    ];
    const response = await dbQuery(updateUser, values);
    const dbResult = response.rows[0];
    delete dbResult.password;
    successMessage.data = dbResult;
    return res.status(status.success).send(successMessage);
  } catch (error) {
    errorMessage.error = "Failed to update user.";
    return res.status(status.error).send(errorMessage);
  }
};

export {
  createAdmin,
  updateUserToAdmin
};