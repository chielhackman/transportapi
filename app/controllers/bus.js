import moment from "moment";
import dbQuery from "../db/dev/dbQuery";
import isEmpty from "../helpers/validations";
import {
  errorMessage, successMessage, status
} from "../helpers/status";

const addBusDetails = async (req, res) => {
  const {
    number_plate, manufacturer, model, year, capacity
  } = req.body;
  
  const { is_admin } = req.user;
  if (!is_admin === true) {
    errorMessage.error = 'Unauthorized';
    return res.status(status.bad).send(errorMessage);
  }

  const created_at = moment(new Date());
  
  // This isn't a good approach, if there are added more attributes to the admin the list will grow
  // Think about passing an array to empty method and loop through it and give back the empty
  // attribute so we can display a proper error message
  try {
    isEmpty(number_plate);
    isEmpty(manufacturer);
    isEmpty(model);
    isEmpty(year);
    isEmpty(capacity);
  } catch (err) {
    errorMessage.error = "All fields are required.";
    return res.status(status.bad).send(errorMessage);
  }
  
  const createBusQuery = `INSERT INTO
    bus(number_plate, manufacturer, model, year, capacity, created_at)
    VALUES($1, $2, $3, $4, $5, $6)
    returning *`;
  const values = [
    number_plate,
    manufacturer,
    model,
    year,
    capacity,
    created_at
  ];

  try {
    const { rows } = await dbQuery.query(createBusQuery, values);
    const dbResponse = rows[0];
    successMessage.data = dbResponse;
  } catch (error) {
    errorMessage.error = "Failed to add bus";
    return res.status(status.error).send(errorMessage);
  }
  return res.status(status.created).send(successMessage);
};

const getAllBuses = async (req, res) => {
  const getAllBusQuery = `SELECT * FROM bus ORDER BY id DESC`;
  try {
    const { rows } = await dbQuery.query(getAllBusQuery);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      errorMessage.error = "There are no buses";
      return res.status(status.notfound).send(errorMessage);
    }
    successMessage.data = dbResponse;
    return res.status(status.success).send(successMessage);
  } catch (error) {
    errorMessage.error = "Failed to get all buses";
    return res.status(status.error).send(errorMessage);
  }
};

export {
  addBusDetails,
  getAllBuses
};