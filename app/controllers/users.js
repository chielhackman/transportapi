import moment from "moment";
import dbQuery from "../db/dev/dbQuery";
import {
  hashPassword,
  comparePassword,
  isValidEmail,
  validatePassword,
  isEmpty,
  generateUserToken
} from "../helpers/validations";
import {
  errorMessage, successMessage, status
} from '../../src/lib/helpers/status';

const createUser = async (req, res) => {
  const {
    email, password, first_name, last_name
  } = req.body;

  const created_at = moment(new Date());

  // This isn't a good approach, if there are added more attributes to the admin the list will grow
  // Think about passing an array to isEmpty method and loop through it and give back the empty
  // attribute so we can display a proper error message
  if (isEmpty(email) || isEmpty(password) || isEmpty(first_name) || isEmpty(last_name)) {
    errorMessage.error = "Email, password, first name and last name field can not be empty.";
    return res.status(status.bad).send(errorMessage);
  }

  if (!isValidEmail(email)) {
    errorMessage.error = "Provided email is not valid.";
    return res.status(status.bad).send(errorMessage);
  }

  if (!validatePassword(password)) {
    errorMessage.error = "Password must be more than 5 characters.";
    return res.status(status.bad).send(errorMessage);
  }

  const hashedPassword = hashPassword(password);
  const createUserQuery = `INSERT INTO
    users(email, password, first_name, last_name, created_at)
    VALUES($1, $2, $3, $4, $5)
    returning *`;
  const values = [
    email,
    hashedPassword,
    first_name,
    last_name,
    created_at
  ];

  try {
    const { rows } = await dbQuery.query(createUserQuery, values);
    const dbResponse = rows[0];
    delete dbResponse.password;
    const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
    successMessage.data = dbResponse;
    successMessage.data.token = token;
    return res.status(status.created).send(successMessage);
  } catch (error) {
    if (error.routine === "_bt_check_unique") {
      errorMessage.error = "There already is an account with the provided email.";
      return res.status(status.conflict).send(errorMessage);
    }
    errorMessage.error = "Failed to create a user.";
    return res.status(status.error).send(errorMessage);
  }
};

const signinUser = async (req, res) => {
  const { email, password } = req.body;
  if (isEmpty(email) || isEmpty(password)) {
    errorMessage.error = "Please fill in email and password.";
    return res.status(status.bad).send(errorMessage);
  }
  if (!isValidEmail(email) || !validatePassword(password)) {
    errorMessage.error = "Please enter valid email and password.";
    return res.status(status.bad).send(errorMessage);
  }
  const signinUserQuery = `SELECT * FROM users WHERE email = $1`;
  try {
    const { rows } = await dbQuery.query(signinUserQuery, [email]);
    const dbResponse = rows[0];
    if (!dbResponse) {
      errorMessage.error = "Unknown provided email.";
      return res.status(status.notfound).send(errorMessage);
    }
    if (!comparePassword(dbResponse.password, password)) {
      errorMessage.error = "Email and password don't match.";
      return res.status(status.bad).send(errorMessage);
    }
    const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
    delete dbResponse.password;
    successMessage.data = dbResponse;
    successMessage.data.token = token;
    return res.status(status.success).send(successMessage);
  } catch (error) {
    errorMessage.error = "Failed to signin.";
    return res.status(status.error).send(errorMessage);
  }
};

export {
  createUser,
  signinUser
};